﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Mapping;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using OnlineCheckInSystem.Common;
using OnlineCheckInSystem.Models;

namespace OnlineCheckInSystem.Controllers
{
    /// <summary>
    /// 学生管理
    /// </summary>
    public class StudentController : Controller
    {
        // GET: Student
        private OcDbContext db = new OcDbContext();

        [HttpGet]
        public ActionResult Index()
        {
            if (Session["TeacherId"] == null || Session["TeacherId"].ToString() =="")
            {
                return RedirectToAction("Index", "Login");
            }

            var list = db.StudentInfo.ToList();
            return View(list);
        }

        [HttpPost]
        public ActionResult GetList(string studentName="", string studentNumber="", int pageIndex = 1,
            int pageSize = int.MaxValue)
        {
            try
            {
                var userId = Session["TeacherId"];
                var dto =
                    from studentInfo in db.StudentInfo
                    select studentInfo;

                if (!string.IsNullOrEmpty(studentName))
                {
                    dto = dto.Where(m => m.StudentName.Contains(studentName));
                }
                if (!string.IsNullOrEmpty(studentNumber))
                {
                    dto = dto.Where(m => m.StudentNumber.Contains(studentNumber));
                }
                var total = dto.Count();

                var list = dto.OrderBy(m => m.Id).Skip(pageSize * (pageIndex - 1)).Take(pageSize).AsNoTracking().ToList();

                var pageNav = PageHyperlinkHelper.ShowPageNav(pageIndex, total, pageSize, "getlist");

                return Json(new { result = true, msg = "请求成功", data = list, pageNav = pageNav });
            }
            catch (Exception e)
            {
                return Json(new { result = false, msg = "请求失败" });
            }
        }

        [HttpGet]
        public ActionResult AddOrEdit(int id = 0)
        {
            if (Session["TeacherId"] == null || Session["TeacherId"].ToString() == "")
            {
                return RedirectToAction("Index", "Login");
            }
            StudentInfo model;
            if (id <= 0)
            {
                model = new StudentInfo();
            }
            else
            {
                model = db.StudentInfo.FirstOrDefault(m => m.Id == id);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult AddOrEdit(string name, string number, int id = 0)
        {
            try
            {
                if (db.StudentInfo.Any(m => m.StudentNumber == number && id != m.Id))
                {
                    return Json(new ResultModel() { msg = "该学号已存在", result = false });
                }

                if (id <= 0)
                {

                    StudentInfo student = new StudentInfo()
                    {
                        StudentNumber = number,
                        StudentName = name,
                        StudentPassword = "123456".ToMD5(),
                        StudentToken = "1234".ToMD5()
                    };

                    db.StudentInfo.Add(student);
                }
                else
                {
                    StudentInfo student =db.StudentInfo.Find(id);
                    student.StudentName = name;
                    student.StudentNumber = number;

                }
                db.SaveChanges();

                return Json(new ResultModel() { msg = "操作成功", result = true });
            }
            catch (Exception e)
            {
                return Json(new ResultModel() { msg = e.Message, result = false });
            }
        }

        [HttpPost]
        public ActionResult Save(StudentInfo student)
        {
            try
            {
                //修改
                if (student.Id > 0)
                {
                    if (db.StudentInfo.Any(m => m.StudentNumber == student.StudentNumber && m.Id != student.Id))
                    {
                        return Json(new ResultModel() { result = false, msg = "该学号已存在，请检查" });
                    }

                    var model = db.StudentInfo.Find(student.Id);

                    model.StudentNumber = student.StudentNumber;
                    model.StudentName = student.StudentName;

                    db.SaveChanges();
                }
                else //新增
                {
                    if (db.StudentInfo.Any(m => m.StudentNumber == student.StudentNumber))
                    {
                        return Json(new ResultModel() { result = false, msg = "该学号已存在，请检查" });
                    }

                    db.StudentInfo.Add(student);

                    db.SaveChanges();
                }

                return Json(new ResultModel() { result = true, msg = "操作成功" });
            }
            catch (Exception e)
            {
                return Json(new ResultModel() { result = true, msg = "操作失败，" + e.Message });
            }
        }

        [HttpPost]
        public ActionResult Delete(int id = 0)
        {
            try
            {
                var model = db.StudentInfo.FirstOrDefault(m => m.Id == id);
                if (model != null && model.Id == 0)
                {
                    return Json(new ResultModel() { result = true, msg = "操作失败" });
                }

                db.StudentInfo.Remove(model);
                db.SaveChanges();

                return Json(new ResultModel() { result = true, msg = "操作成功" });
            }
            catch (Exception e)
            {
                return Json(new ResultModel() { result = true, msg = "操作失败，" + e.Message });
            }
        }
    }
}