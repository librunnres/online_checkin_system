﻿using OnlineCheckInSystem.Common;
using OnlineCheckInSystem.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace OnlineCheckInSystem.Controllers
{
    public class MobileController : Controller
    {
        private OcDbContext db = new OcDbContext();

        // GET: Mobile
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string number, string password)
        {
            var model = db.StudentInfo.FirstOrDefault(m => m.StudentNumber == number);

            if (model == null || model.Id <= 0)
            {
                return Json(new ResultModel() { msg = "学号不存在", result = false });
            }

            if (model.StudentPassword != password.ToMD5())
            {
                return Json(new ResultModel() { msg = "密码错误", result = false });
            }

            Session["Id"] = model.Id;
            Session["StudentName"] = model.StudentName;
            Session["StudentNumber"] = model.StudentNumber;

            return Json(new ResultModel() { msg = "登录成功，正在跳转", result = true });
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(string number, string password, string name, string token)
        {
            try
            {
                if (db.StudentInfo.Any(m => m.StudentNumber == number))
                {
                    return Json(new ResultModel() { msg = "该学号已存在", result = false });
                }

                StudentInfo student = new StudentInfo()
                {
                    StudentNumber = number,
                    StudentName = name,
                    StudentPassword = password.ToMD5(),
                    StudentToken = token.ToMD5()
                };

                db.StudentInfo.Add(student);

                db.SaveChanges();

                return Json(new ResultModel() { msg = "注册成功，请登录", result = true });
            }
            catch (Exception e)
            {
                return Json(new ResultModel() { msg = e.Message, result = false });
            }
        }

        public ActionResult ForgetPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgetPassword(string number, string password, string name, string token)
        {
            try
            {
                var model = db.StudentInfo.FirstOrDefault(m => m.StudentNumber == number);
                if (model == null || model.Id <= 0)
                {
                    return Json(new ResultModel() { msg = "该学号不存在", result = false });
                }
                if (model.StudentName != name)
                {
                    return Json(new ResultModel() { msg = "姓名输入错误", result = false });
                }
                if (model.StudentToken != token.ToMD5())
                {
                    return Json(new ResultModel() { msg = "验证码输入错误", result = false });
                }

                model.StudentPassword = password.ToMD5();

                db.SaveChanges();

                return Json(new ResultModel() { msg = "密码修改成功，请登录", result = true });
            }
            catch (Exception e)
            {
                return Json(new ResultModel() { msg = e.Message, result = false });
            }
        }

        [HttpGet]
        public ActionResult Index()
        {
            if (Session["Id"] == null || Session["Id"] == "")
            {
                return RedirectToAction("Login");
            }

            var id = int.Parse(Session["Id"].ToString());

            var student = db.StudentInfo.Find(id);
            var list = db.SeatInfo.Where(m => m.StudentId == student.Id).OrderByDescending(m => m.Id).ToList();
            if (list.Count > 0)
            {
                var model = list[0];

                ViewBag.Status = student.StudentStatus;
                ViewBag.SeatNumber = model.SeatNumber;
            }
            else
            {
                ViewBag.Status = student.StudentStatus;
                ViewBag.SeatNumber = "";
            }

            return View();
        }

        [HttpPost]
        public ActionResult Vacate(string begin, string end, string remark)
        {
            try
            {
                var id = int.Parse(Session["Id"].ToString());

                VacateInfo model = new VacateInfo();

                model.BeginDate = DateTime.Parse(begin);
                model.EndDate = DateTime.Parse(end);
                model.Remark = remark;
                model.State = 0;
                model.StudentId = id;
                model.CreateDate = DateTime.Now;

                db.VacateInfo.Add(model);

                db.SaveChanges();

                return Json(new ResultModel() { msg = "请假成功", result = true });
            }
            catch (Exception e)
            {
                return Json(new ResultModel() { msg = e.Message, result = false });
            }
        }

        public ActionResult SignIn(string seatNumber)
        {
            try
            {
                //通过座位号查询 是否有该座位号
                var model = db.SeatInfo.FirstOrDefault(m => m.SeatNumber == seatNumber);

                if (model == null || model.Id <= 0)
                {
                    return Json(new ResultModel() { msg = "座位号不存在，请重新输入", result = false });
                }

                if (model.StudentId > 0)
                {
                    return Json(new ResultModel() { msg = "该座位已有人签到，请换座位", result = false });
                }

                var id = int.Parse(Session["Id"].ToString());

                model.StudentId = id;
                
                SignInfo sign = new SignInfo()
                {
                    SignDate = DateTime.Now,
                    StudentId = id,
                    VacateId = model.Id
                };

                db.SignInfo.Add(sign);

                var student = db.StudentInfo.Find(id);
                student.StudentStatus = 1;

                db.SaveChanges();

                return Json(new ResultModel() { msg = "签到成功", result = true });
            }
            catch (Exception e)
            {
                return Json(new ResultModel() { msg = e.Message, result = false });
            }
        }
    }
}