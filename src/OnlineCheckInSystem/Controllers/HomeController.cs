﻿using System;
using System.Linq;
using OnlineCheckInSystem.Models;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;

namespace OnlineCheckInSystem.Controllers
{
    public class HomeController : Controller
    {
        private OcDbContext db = new OcDbContext();

        #region 首页
        [HttpGet]
        public ActionResult Index()
        {
            if (Session["TeacherId"] == null || Session["TeacherId"].ToString() == "")
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult GetList()
        {
            var list = db.SeatInfo.Include("StudentInfo").Where(m => m.ClassRoomId == 1).ToList();

            return Json(new ResultModel() { result = true, data = list });
        }

        /// <summary>
        /// 获取当前实时人数
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetCurrentData()
        {
            var list = db.SeatInfo.Include("StudentInfo").Where(m => m.ClassRoomId == 1 && m.StudentId > 0).ToList();

            return Json(new ResultModel() { result = true, data = list });
        }

        /// <summary>
        /// 一键释放
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ShiFang()
        {
            try
            {
                var listStudent = db.StudentInfo.ToList();

                foreach (var studentInfo in listStudent)
                {
                    studentInfo.StudentStatus = 0;
                }

                var listSeat = db.SeatInfo.ToList();

                foreach (var seatInfo in listSeat)
                {
                    seatInfo.StudentId = 0;
                }

                db.SaveChanges();

                return Json(new ResultModel() { result = true });
            }
            catch (Exception e)
            {
                return Json(new ResultModel() { result = false, msg = e.Message });
            }
        }

        /// <summary>
        /// 获取实时人数
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetNumber()
        {
            try
            {
                var student = db.StudentInfo.Count();
                var shidao = db.SeatInfo.Count(m => m.StudentId != null && m.StudentId > 0);
                var qingjia = db.VacateInfo.Count(m => m.BeginDate <= DateTime.Now && m.EndDate >= DateTime.Now);
                var weidao = student - shidao - qingjia;
                return Json(new ResultModel() { result = true, data = new { total = student, shidao = shidao, qingjia = qingjia, weidao = weidao } });
            }
            catch (Exception e)
            {
                return Json(new ResultModel() {result = false, msg = e.Message});
            }
        }
        #endregion 首页
    }
}