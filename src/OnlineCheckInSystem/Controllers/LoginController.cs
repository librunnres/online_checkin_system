﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using OnlineCheckInSystem.Common;
using OnlineCheckInSystem.Models;

namespace OnlineCheckInSystem.Controllers
{
    /// <summary>
    /// 教师端登录界面
    /// </summary>
    public class LoginController : Controller
    {
        private OcDbContext db = new OcDbContext();

        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login(string loginName, string passWord)
        {
            if (string.IsNullOrEmpty(loginName) || string.IsNullOrEmpty(passWord))
            {
                return Json(new ResultModel() { result = false, msg = "账号或密码不能为空" });
            }

            var teacher = db.TeacherInfo.FirstOrDefault(m => m.LoginName == loginName);

            if (teacher == null || teacher.Id == 0)
            {
                return Json(new ResultModel() { result = false, msg = "账号不存在" });
            }

            if (teacher.UserPassword != passWord.ToMD5())
            {
                return Json(new ResultModel() { result = false, msg = "登录密码错误" });
            }
            
            Session["TeacherName"] = teacher.TeacherName;
            Session["TeacherId"] = teacher.Id;

            return Json(new ResultModel() { result = true, msg = "登录成功" });
        }
    }
}