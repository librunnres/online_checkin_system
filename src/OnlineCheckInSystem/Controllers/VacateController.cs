﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineCheckInSystem.Models;

namespace OnlineCheckInSystem.Controllers
{
    public class VacateController : Controller
    {
        OcDbContext db = new OcDbContext();
        // GET: Vacate
        public ActionResult Index()
        {
            if (Session["TeacherId"] == null || Session["TeacherId"].ToString() == "")
            {
                return Redirect("/Login/Index");
            }
            var list = db.VacateInfo.Include("StudentInfo").Where(m => m.State == 0).ToList();
            return View(list);
        }

        [HttpPost]
        public ActionResult ShenPi(int id, int kind)
        {
            try
            {
                var model = db.VacateInfo.Find(id);

                if (model == null || model.Id <= 0)
                {
                    return Json(new ResultModel() { msg = "操作失败，请稍后重试", result = false });
                }

                model.State = kind;
                model.TeacherId = int.Parse(Session["TeacherId"].ToString());

                db.SaveChanges();


                return Json(new ResultModel() { msg = "操作成功", result = true });
            }
            catch (Exception e)
            {
                return Json(new ResultModel() { msg = "操作失败", result = false });
            }
        }
    }
}