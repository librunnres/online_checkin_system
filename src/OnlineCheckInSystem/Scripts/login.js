﻿$(function () {
    $("#btnLogin").click(function () {
        var userName = $("#txtLoginName").val().trim();
        var password = $("#txtLoginPwd").val().trim();
        if (userName.length <= 0) {
            layer.msg("登录账号不能为空");
            return false;
        }
        if (password.length <= 0) {
            layer.msg("登录密码不能为空");
            return false;
        }
        $.ajax({
            type: 'post',
            url: "/Login/Login",
            data: {
                loginName: userName,
                passWord: password
            },
            success: function (data) {
                if (data.result) {
                    window.location = "/Home";
                } else {
                    layer.msg(data.msg);
                    return false;
                }
            },
            error: function () {
                layer.msg("请求出错");
                return false;
            }
        });
    });
});