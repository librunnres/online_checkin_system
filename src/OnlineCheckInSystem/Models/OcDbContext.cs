﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Web.UI.WebControls;
using Microsoft.Ajax.Utilities;

namespace OnlineCheckInSystem.Models
{
    public class OcDbContext : DbContext
    {
        public OcDbContext() : base("ConStr")
        {
            Database.SetInitializer<OcDbContext>(null);
        }

        public virtual DbSet<ClassInfo> ClassInfo { get; set; }
        public virtual DbSet<ClassRoomInfo> ClassRoomInfo { get; set; }
        public virtual DbSet<SeatInfo> SeatInfo { get; set; }
        public virtual DbSet<SignInfo> SignInfo { get; set; }
        public virtual DbSet<StudentInfo> StudentInfo { get; set; }
        public virtual DbSet<TeacherInfo> TeacherInfo { get; set; }
        public virtual DbSet<VacateInfo> VacateInfo { get; set; }
    }
}