﻿namespace OnlineCheckInSystem.Models
{
    public class ResultModel
    {
        public bool result { get; set; }
        public string msg { get; set; }
        public object data { get; set; }
    }
}