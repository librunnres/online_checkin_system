﻿using OnlineCheckInSystem.Models;
using System.Web.Mvc;
using System.Web.Security;

namespace OnlineCheckInSystem.Filter
{
    /// <summary>
    /// 身份认证过滤器
    /// </summary>
    public class AuthFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            bool isAnoy = filterContext.ActionDescriptor.IsDefined(typeof(AuthFilter), true) ||
                filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AuthFilter), true);

            var identity = filterContext.HttpContext.User.Identity;

            if (isAnoy && !identity.IsAuthenticated)
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    var result = new ResultModel
                    {
                        result = false,
                        msg = "请登录",
                    };
                    filterContext.Result = new JsonResult
                    {
                        Data = result,
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                else
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
        }
    }
}