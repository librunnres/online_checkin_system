﻿using System;
using System.Text;

namespace OnlineCheckInSystem.Common
{
    public static class PageHyperlinkHelper
    {
        /// <summary>
        /// 获取分页标签的字符串
        /// </summary>
        /// <param name="pageCurrent">当前页</param>
        /// <param name="totalCount">数据总条数</param>
        /// <param name="pageSize">页容量</param>
        /// <returns></returns>
        public static string ShowPageNav(int pageCurrent, int totalCount, int pageSize = 15, string methodName = "getlist")
        {
            //总页数
            var totalPage = Math.Max((totalCount + pageSize - 1) / pageSize, 1);
            var homePage = "首页";
            var lastPage = "末页";
            var previousPage = "上一页";
            var nextPage = "下一页";

            //要输出的超链接字符串
            var pageNav = new StringBuilder();

            //判断总页数是不是大于一 如果是 则有首页和上一页
            if (totalPage > 1)
            {
                //如果当前页是第一页，则没有首页
                if (pageCurrent != 1)
                {
                    pageNav.AppendFormat("<li class='paginate_button'><a href='javascript:void(0)' onclick='" + methodName + "({0},{1})'>{2}</a></li>", 1, pageSize, homePage);
                }

                //如果当前页大于第一页，才有第一页
                if (pageCurrent > 1)
                {
                    pageNav.AppendFormat("<li class='paginate_button'><a href='javascript:void(0)' onclick='" + methodName + "({0},{1})'>{2}</a></li>", pageCurrent - 1, pageSize, previousPage);
                }

                int current = 4;

                for (int i = 1; i <= 8; i++)
                {
                    //一共显示7页，前面3页后面3页 中间显示当前页
                    if ((pageCurrent + i - current) >= 1 && (pageCurrent + i - current) <= totalPage)
                    {
                        if (current == i) //当前页处理
                        {
                            pageNav.AppendFormat("<li class='paginate_button'><a href='javascript:void(0)' style='background-color:#eee;'>{0}</a></li>", pageCurrent);
                        }
                        else //一般页处理
                        {
                            pageNav.AppendFormat("<li class='paginate_button'><a href='javascript:void(0)' onclick='" + methodName + "({0},{1})'>{2}</a></li>", pageCurrent + i - current, pageSize, pageCurrent + i - current);
                        }
                    }
                }

                //如果当前页小于总页数，才有下一页
                if (pageCurrent < totalPage)
                {
                    pageNav.AppendFormat("<li class='paginate_button'><a href='javascript:void(0)' onclick='" + methodName + "({0},{1})'>{2}</a></li>", pageCurrent + 1, pageSize, nextPage);
                }

                //如果当前页是最后一页，则没有末页
                if (pageCurrent != totalPage)
                {
                    pageNav.AppendFormat("<li class='paginate_button'><a href='javascript:void(0)' onclick='" + methodName + "({0},{1})'>{2}</a></li>", totalPage, pageSize, lastPage);
                }
                //第几页 / 共几页
                pageNav.AppendFormat("<li class='paginate_button'><a href='javascript:void(0)'>第{0}页 / 共{1}页 共{2}条</a></li>", pageCurrent, totalPage, totalCount);
                return pageNav.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }
}